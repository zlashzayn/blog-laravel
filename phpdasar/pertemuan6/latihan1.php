<?php
$buku = [
    [
        "id" => "00001",
        "judul" => "Harry Potter 1",
        "tahun" => "1990",
        "penulis" => "J.K. Rolling",
        "gambar" => "harry potter.jpg"
    ],
    [
        "id" => "00002",
        "judul" => "The Lord of The Rings",
        "tahun" => "1954",
        "penulis" => "J.R.R. Tolkien",
        "gambar" => "tlotr.jpg"
    ]
]
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Daftar Buku</title>
</head>
<body>
    <h1>Daftar Buku</h1>
    <?php foreach ($buku as $book) : ?>
        <ul>
            <li>
                <img src="img/<?php $book["gambar"]; ?>">
            </li>
            <li>ID Buku : <?php $book["id"]; ?></li>
            <li>Judul : <?php $book["judul"]; ?></li>
            <li>Tahun Terbit : <?php $book["tahun"]; ?></li>
            <li>Penulis : <?php $book["penulis"]; ?></li>
        </ul>
    <?php endforeach; ?>
</body>
</html>