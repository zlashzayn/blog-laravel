<?php 
$buku = [
    [
        "id" => "00001",
        "judul" => "Harry Potter 1",
        "tahun" => "1990",
        "penulis" => "J.K. Rolling",
        "gambar" => "harry potter.jpg"
    ],
    [
        "id" => "00002",
        "judul" => "The Lord of The Rings",
        "tahun" => "1954",
        "penulis" => "J.R.R. Tolkien",
        "gambar" => "tlotr.jpg"
    ]
]
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Daftar Buku</title>
</head>
<body>
    <h1>Daftar Buku</h1>
    <ul>
        <?php foreach ($buku as $books) : ?>
            <li>
                <a href="latihan2.php?&id=<?= $books["id"]; ?>&judul=<?= $books["judul"]; ?>&tahun=<?= $books["tahun"]; ?>&penulis=<?= $books["penulis"]; ?>"><?= $books["judul"]; ?></a>
            </li>
        <?php endforeach; ?>
    </ul>
</body>
</html>