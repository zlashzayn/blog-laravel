<?php 
// cek apakah tidak ada data di $_GET
if (!isset($_GET["judul"]) || !isset($_GET["id"]) || !isset($_GET["tahun"]) || !isset($_GET["penulis"])) {
    // redirect
    header("Location: latihan1.php");
    exit;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Detail Buku</title>
</head>
<body>
    <ul>
        <li><?= $_GET["id"]; ?></li>
        <li><?= $_GET["judul"]; ?></li>
        <li><?= $_GET["tahun"]; ?></li>
        <li><?= $_GET["penulis"]; ?></li>
    </ul>

    <a href="latihan1.php">Kembali ke Daftar Buku</a>
</body>
</html>