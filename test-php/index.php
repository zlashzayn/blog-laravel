<?php 

function xo($str) {
    if (strlen("x") == strlen("o")) {
        echo "Benar";
    }
    else {
        echo "Salah";
    }
}

echo xo("xoxoxo");
echo xo("oxooxo");
echo xo("oxo");
echo xo("xxooox");
echo xo("xoxooxxo");
echo "<br>";

function pagar_bintang($integer) {
    for ($i = 0; $i < $integer; $i++) {
        for ($j = 0; $j < $integer; $j++) {
            if ($i % 2 == 0) {
                echo "#";
            }
            else {
                echo "*";
            }
        }
        echo "<br>";
    }
    echo "<br>";
}

echo pagar_bintang(5);
echo pagar_bintang(8);
echo pagar_bintang(10);

?>