<?php

namespace App\Models;

class Post
{
    private static $blog_posts = [
        [
            "title" => "Judul Post Pertama",
            "slug" => "judul-post-pertama",
            "author" => "Zainul Fuadi",
            "body" => "Lorem ipsum dolor sit amet consectetur adipisicing elit. Deserunt omnis ut libero maxime sunt, blanditiis autem enim quisquam eius explicabo ducimus dolorem odit excepturi, recusandae tenetur dolor dicta ea ex natus? Eos officia voluptatibus nulla minus quidem quas excepturi maiores vel, neque quaerat illum reiciendis voluptas veniam soluta est asperiores fugiat ea. Voluptatum eligendi laborum velit quasi, animi deleniti impedit praesentium cupiditate ratione voluptates quisquam nobis nihil, quae corporis? Voluptatem aliquam ipsa perspiciatis itaque deserunt deleniti accusamus porro, maxime nulla?"
        ],
        [
            "title" => "Judul Post Kedua",
            "slug" => "judul-post-kedua",
            "author" => "LS",
            "body" => "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Vel ea officia illo sed saepe tenetur molestiae necessitatibus sunt incidunt et dolorem voluptatem eos doloribus beatae quaerat corporis perspiciatis, optio similique ipsum dicta suscipit nostrum minima reprehenderit facere. Inventore eos impedit, tempora commodi nesciunt quos, beatae eligendi eveniet ea quam eum accusamus quae reprehenderit optio sed dicta error quia officia totam doloremque facilis, enim ipsum reiciendis. Ducimus, repellat quam. Dolor tenetur repudiandae iusto voluptatem consequatur ea reprehenderit. Illum quo, aspernatur laboriosam iusto consequatur suscipit impedit esse saepe harum quae quis aut, aperiam dolore repellendus doloremque laudantium temporibus ut unde, a eaque!"
        ]
    ];

    public static function all() {
        return collect(self::$blog_posts);
    }

    public static function find($slug) {
        $posts = static::all();
        return $posts->firstWhere('slug',$slug);
    }
}
