<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Category;
use App\Models\Post;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::factory(3)->create();

        // User::create([
        //     'name' => 'Zainul Fuadi',
        //     'email' => 'zain@gmail.com',
        //     'password' => bcrypt('12345')
        // ]);

        // User::create([
        //     'name' => 'LS',
        //     'email' => 'lshaha@gmail.com',
        //     'password' => bcrypt('12345')
        // ]);

        Category::create([
            'name' => 'Web Programming',
            'slug' => 'web-programming'
        ]);
        
        Category::create([
            'name' => 'Personal',
            'slug' => 'personal'
        ]);

        Post::factory(20)->create();
        
        // Post::create([
        //     'title' => 'Judul Pertama',
        //     'slug' => 'judul-pertama',
        //     'excerpt' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestiae, perspiciatis? Sequi inventore at molestias nesciunt sint soluta accusamus',
        //     'body' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestiae, perspiciatis? Sequi inventore at molestias nesciunt sint soluta accusamus dolor nulla reiciendis earum. Nemo similique quasi autem cumque, animi adipisci sint rerum esse consequuntur repudiandae mollitia consectetur, nobis quisquam magni consequatur! Unde ipsam voluptatum, iste atque impedit dolor. Minima aut commodi enim sed deserunt eos impedit numquam, doloremque dolores cum illum fugit modi harum necessitatibus eligendi quis voluptatum cupiditate doloribus nisi quo ducimus ipsa suscipit. Maiores, deleniti. Ullam, ipsa ea rem dolor similique alias obcaecati quo laboriosam aliquid debitis quidem, nulla exercitationem incidunt beatae voluptas. Quidem cum enim distinctio quod, perferendis suscipit. Voluptates nisi repellat facilis, ipsum, facere nam quam tempore quia sunt fuga magni? Nesciunt laudantium eveniet natus aut sed.',
        //     'category_id' => 1,
        //     'user_id' => 1
        // ]);
        // Post::create([
        //     'title' => 'Judul Kedua',
        //     'slug' => 'judul-kedua',
        //     'excerpt' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestiae, perspiciatis? Sequi inventore at molestias nesciunt sint soluta accusamus',
        //     'body' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestiae, perspiciatis? Sequi inventore at molestias nesciunt sint soluta accusamus dolor nulla reiciendis earum. Nemo similique quasi autem cumque, animi adipisci sint rerum esse consequuntur repudiandae mollitia consectetur, nobis quisquam magni consequatur! Unde ipsam voluptatum, iste atque impedit dolor. Minima aut commodi enim sed deserunt eos impedit numquam, doloremque dolores cum illum fugit modi harum necessitatibus eligendi quis voluptatum cupiditate doloribus nisi quo ducimus ipsa suscipit. Maiores, deleniti. Ullam, ipsa ea rem dolor similique alias obcaecati quo laboriosam aliquid debitis quidem, nulla exercitationem incidunt beatae voluptas. Quidem cum enim distinctio quod, perferendis suscipit. Voluptates nisi repellat facilis, ipsum, facere nam quam tempore quia sunt fuga magni? Nesciunt laudantium eveniet natus aut sed.',
        //     'category_id' => 1,
        //     'user_id' => 1
        // ]);
        // Post::create([
        //     'title' => 'Judul Ketiga',
        //     'slug' => 'judul-ketiga',
        //     'excerpt' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestiae, perspiciatis? Sequi inventore at molestias nesciunt sint soluta accusamus',
        //     'body' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestiae, perspiciatis? Sequi inventore at molestias nesciunt sint soluta accusamus dolor nulla reiciendis earum. Nemo similique quasi autem cumque, animi adipisci sint rerum esse consequuntur repudiandae mollitia consectetur, nobis quisquam magni consequatur! Unde ipsam voluptatum, iste atque impedit dolor. Minima aut commodi enim sed deserunt eos impedit numquam, doloremque dolores cum illum fugit modi harum necessitatibus eligendi quis voluptatum cupiditate doloribus nisi quo ducimus ipsa suscipit. Maiores, deleniti. Ullam, ipsa ea rem dolor similique alias obcaecati quo laboriosam aliquid debitis quidem, nulla exercitationem incidunt beatae voluptas. Quidem cum enim distinctio quod, perferendis suscipit. Voluptates nisi repellat facilis, ipsum, facere nam quam tempore quia sunt fuga magni? Nesciunt laudantium eveniet natus aut sed.',
        //     'category_id' => 2,
        //     'user_id' => 1
        // ]);
        // Post::create([
        //     'title' => 'Judul Keempat',
        //     'slug' => 'judul-keempat',
        //     'excerpt' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestiae, perspiciatis? Sequi inventore at molestias nesciunt sint soluta accusamus',
        //     'body' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestiae, perspiciatis? Sequi inventore at molestias nesciunt sint soluta accusamus dolor nulla reiciendis earum. Nemo similique quasi autem cumque, animi adipisci sint rerum esse consequuntur repudiandae mollitia consectetur, nobis quisquam magni consequatur! Unde ipsam voluptatum, iste atque impedit dolor. Minima aut commodi enim sed deserunt eos impedit numquam, doloremque dolores cum illum fugit modi harum necessitatibus eligendi quis voluptatum cupiditate doloribus nisi quo ducimus ipsa suscipit. Maiores, deleniti. Ullam, ipsa ea rem dolor similique alias obcaecati quo laboriosam aliquid debitis quidem, nulla exercitationem incidunt beatae voluptas. Quidem cum enim distinctio quod, perferendis suscipit. Voluptates nisi repellat facilis, ipsum, facere nam quam tempore quia sunt fuga magni? Nesciunt laudantium eveniet natus aut sed.',
        //     'category_id' => 2,
        //     'user_id' => 2
        // ]);
    }
}
