<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Form</title>
    </head>
    <body>
        <h1>Buat Account Baru</h1>
        
        <h3>Sign Up Form</h3>

        <form action="welcome" method="GET">
            <label for="fname">First Name:</label><br>
            <input type="text" id="fname" name="fname"><br>
            <label for="lname">Last Name:</label><br>
            <input type="text" id="lname" name="lname"><br>

            <p>Gender:</p>
            <input type="radio" id="male" name="gender" value="Male">
            <label for="male">Male</label><br>
            <input type="radio" id="female" name="gender" value="Female">
            <label for="female">Male</label><br>
            <input type="radio" id="other" name="gender" value="Other">
            <label for="other">Other</label><br>

            <p>Nationality:</p>
            <select id="nationality" name="nationality">
                <option value="indonesian">Indonesian</option>
                <option value="malaysian">Malaysian</option>
                <option value="singaporean">Singaporean</option>
                <option value="australian">Australian</option>
            </select>
            <br>

            <p>Language Spoken:</p>
            <input type="checkbox" id="indo" name="indo" value="Bahasa Indonesia">
            <label for="indo">Bahasa Indonesia</label><br>
            <input type="checkbox" id="english" name="english" value="English">
            <label for="english">English</label><br>
            <input type="checkbox" id="other" name="other" value="Other">
            <label for="other">Other</label><br>

            <p>Bio:</p>
            <textarea name="bio" id="bio" cols="30" rows="10"></textarea><br>
            <input type="submit" value="Submit">
        </form>
    </body>
</html>